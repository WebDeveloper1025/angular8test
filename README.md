
# Installation Guide

## Implemented content:
* Created a img element with invalid image source. This project check image src if that src is valid. If src is invalid that image element loads `assets\img\no-image.png` file.
* Created a table that shows the data from a mock json Server.
* Created a table that has functions like Next/Prev page, Limit selection(Change the number of the row), Move to a specific page with pagination.
  That table also have search element.
  sample mockup server API url:https://2a8b32e0-a555-48e0-a648-8fb6a570c12c.mock.pstmn.io/api/v1/test-data

## Retrieve code

* `$ git clone https://github.com/`


## Installation

### Install dependencies

* Install [NodeJS](https://nodejs.org/en/), [NPM](https://www.npmjs.com/)
* `$ npm install`
* `$ npm install -g yarn`
* `$ npm install -g gulp`

### Run and Compile Project

* `$ cd tools`
* `$ yarn`
* `$ gulp --angular`
* `$ cd ..`
* `$ npm start`

Then open your browser the page: http://HOST_URL:4200/
