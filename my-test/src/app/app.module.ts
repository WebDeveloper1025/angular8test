import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';    
import {HttpClientModule} from '@angular/common/http'
import {MatPaginatorModule, MatSortModule, MatTableModule, MatFormFieldModule, MatInputModule} from '@angular/material'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { MatDialogModule } from '@angular/material/dialog';
import { from } from 'rxjs';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent,
    TestComponent
  ],
  imports: [
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatTableModule,
    BrowserModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
