import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http"

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  apiURL: string = "https://2a8b32e0-a555-48e0-a648-8fb6a570c12c.mock.pstmn.io/";
  //https://2a8b32e0-a555-48e0-a648-8fb6a570c12c.mock.pstmn.io/api/v1/test-data
  
  constructor(private httpClient: HttpClient) { }

  public getCustomers(){
    return this.httpClient.get(this.apiURL + 'api/v1/test-data');
  }
}
