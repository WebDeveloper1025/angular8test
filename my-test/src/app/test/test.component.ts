import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ApiService} from '../api.service'
import * as $ from 'jquery';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  displayedColumns = ['id', 'name', 'progress', 'color'];
  dataSource: MatTableDataSource<CustomerData>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  imgUrl = "http://example.com/non-existent-image.jpg";
  // imgUrl = "https://images.unsplash.com/photo-1494438639946-1ebd1d20bf85?ixlib=rb-1.2.1&w=1000&q=80"
  customers: CustomerData[] = [];
  serverData;
  constructor(private apiService: ApiService) {
    // this.dataSource2 = new MatTableDataSource(this.customers);
   }

  applyFilter2(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit() {
    let that = this;
    this.apiService.getCustomers().subscribe((data) => {
      that.serverData = data;
      (<any>data).forEach(element => {
        that.customers.push({
          id: element["r-id"],
          city: element["ship-city"],
          country: element["country"],
          date: element["ship-date"]
         });
      });
    this.dataSource = new MatTableDataSource(this.customers);
    console.log(that.customers);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    })
  }
  
  imgUrlChanged(imageUrl){
    this.imgUrl = imageUrl
  }
  onProb3Change(deviceValue) {
    let labelConent = "Search result: City-" + this.customers[deviceValue - 1].city + ", Country-" + this.customers[deviceValue - 1].country + ", Date-" + this.customers[deviceValue - 1].date;
    console.log(labelConent);
    $("#search_result").text(labelConent);
  }
  
  setDefaultPic(){
    this.imgUrl = "assets/img/no-image.png";
  }
}

export interface CustomerData {
  id: number;
  city: string;
  country: string;
  date: string;
}